import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { generateMetaAndUpload } from '../metadata/generate';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // Prepare & upload metadata
  const { metaHash, artworkCount } = await generateMetaAndUpload()

  // const controllerDeploy = await deploy('UniwinesController', {
  //   from: deployer,
  //   args: [],
  //   log: true,
  // });

  let ftAddress
  if (process.env.FT_CONTRACT) {
    ftAddress = process.env.FT_CONTRACT
    console.log("Using existing FT contract:", ftAddress)
  } else {
    const ftDeploy = await deploy('FT', {
      from: deployer,
      args: ['WineSwap VINO', 'VINO', 126],
      log: true,
    });
    ftAddress = ftDeploy.address
  }
  await deploy('NFT', {
    from: deployer,
    args: [
      'WineSwap VERITAS', // Displayname
      'VERITAS', // Token symbol
      ftAddress, // FT address
      `https://cloudflare-ipfs.com/ipfs/${metaHash}/`,
      108, // Maximum supply
      artworkCount, // Artwork count
    ],
    log: true,
  });
};
export default func;
func.tags = ['FT', 'NFT'];