import { Dialog, Transition } from '@headlessui/react'
import { Close } from '@material-ui/icons'
import { Fragment } from 'react'
import { withRouter } from 'react-router-dom'

const CloseIcon = <Close className="text-gray-400 hover:text-gray-50" />
export default withRouter(function NavModal ({ children, buttonText = CloseIcon, backNav = '/', visible = true, setVisible, ...props }) {
  // const cancelButtonRef = useRef()

  // This code makes the demo automatically show on initial render
  // const [, clear] = useTimeoutFn(() => setOpen(true), 750)
  // useEffect(() => {
  //   // clear()
  //   setOpen(true)
  // }, [])

  function closeModal () {
    setVisible && setVisible(false)
    backNav && props.history.push(backNav)
  }

  return (
    <Transition show={visible} as={Fragment}>
      <Dialog
        as="div"
        id="modal"
        className="fixed inset-0 z-10 overflow-y-auto"
        // initialFocus={cancelButtonRef}
        static
        open={visible}
        onClose={closeModal}
        onKeyDown={closeModal}
      >
        <div className="min-h-screen px-4 text-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-black opacity-60" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="inline-block h-screen align-middle"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform text-gray-50  bg-grey-850  shadow-xl rounded-lg">
              {children}

              {!buttonText
                ? null
                : (
                  <div className="mt-4">
                    <button
                      type="button"
                      className="absolute right-2 top-2 inline-flex justify-center p-2 text-sm font-medium rounded-full focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                      onClick={closeModal}
                    >
                      {buttonText}
                    </button>
                  </div>
                  )}

            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  )
})
