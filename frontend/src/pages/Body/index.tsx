import filter from 'lodash/filter'
import { useContext, useMemo, useState } from 'react'
import { Link } from 'react-router-dom'
import { BuyButton, ClaimButton, SellButton, ShipButton } from '../../components/Buttons'
import { TitleH2 } from '../../components/minis'
import Modal from '../../components/Modal'
import Stats from '../../components/Stats'
import { EthBlockContext } from '../../components/Web3ReactManager/block-listener'
import { useAppContext } from '../../context'
import { useEthContext } from '../../context/eth-data'
import { useActiveWeb3React } from '../../hooks/web3'
import eth from '../../img/eth-symbol-virgil.svg'
import question from '../../img/question.svg'
import wine from '../../img/wine_bottle.png' // "//4c3qhu32ws.medianet.work/static/media/wine_bottle.46829ab5.png"
import { amountFormatter, NFT_NAME } from '../../utils/main-utils'
import { rIF } from '../../utils/react-utils'
import Faq from '../Faq'
import Status from '../Status'
import { Footer } from './Footer'
import { Header } from './Header'
import NavModal from './NavModal'

export const FullWidthBarClasses = 'box-border m-0 text-xl text-gray-50 flex flex-row justify-between w-screen max-w-screen h-fit'

export default function Body ({ showStatus, showFAQ }) {
  const { appState, setAppState } = useAppContext()
  const { ethData } = useEthContext()
  const {
    ready,
    ethPrice,
    balanceWINE,
    balanceUNIWINE,
    tokensUNIWINE,
    reserveWineTokens,
    totalSupplyWINE,
  } = ethData
  const block = useContext(EthBlockContext)
  const erosUnredeemed = useMemo(() => tokensUNIWINE && filter(tokensUNIWINE, { shipped: false }), [tokensUNIWINE])
  const redeemedCount = erosUnredeemed && balanceUNIWINE ? balanceUNIWINE.toNumber() - erosUnredeemed.length : null

  const { account } = useActiveWeb3React()
  const [showConnect, setShowConnect] = useState(false)

  return (<>
    <div
      id="Body_AppWrapper"
      className='w-screen min-h-screen max-w-screen my-0 mx-auto flex flex-col flex-wrap justify-stretch items-center text-gray-50 relative overflow-hidden'
      style={{ scrollBehavior: 'smooth' }}
    >
      <Header {...{ setShowConnect }} />
      {appState.verbose
        ? <pre className="bg-grey-700">q
          Block {JSON.stringify(block)}
        </pre>
        : null}

      <img
        src={wine}
        className="absolute -z-2 h-screen mr-8 xs:mr-4 right-1/2 top-24"
        alt="Naga dark wine bottle with stark blue narrow vertical label"
      />

      <div className="flex-auto flex-shrink-0 w-full relative">
        <div id="Content" className="w-1/2 h-fit mr-2 m-0 absolute right-0 xs:left-1/2  bottom-0 xxs:bottom-14 xl:bottom-32 flex flex-row items-end">
          <div className="h-fit w-full flex flex-col items-left">
            <div id="Info" className="mb-4 xxs:mb-12 w-full">
              <div className="mb-2 text-2xl xxs:text-3xl xs:text-5xl tracking-widest">
                {NFT_NAME}
              </div>
              <div className="leading-5">
                A coin. A bottle. <br/>
                A piece of Portugal. <br/>
                A piece of art.
              </div>
            </div>
            {ready
              ? <>
                <div id="MarketData" className="mb-6">
                  <p className="lowercase mb-1.5 text-sm whitespace-nowrap">
                    {reserveWineTokens && totalSupplyWINE
                      ? `${amountFormatter(reserveWineTokens, 18, 0)} available`
                      : ''}
                    <Link className="inline-block  ml-2" to="/faq">
                      <img className="h-3 -mb-0.5" src={question} alt="help / info button" />
                    </Link>
                  </p>
                  <p className="m-0 text-lg xxs:text-3xl tracking-wide">
                    <img
                      src={eth}
                      className="h-8 -mt-1 -ml-2 inline-block"
                      alt="ETH symbol"
                    />
                    {ethPrice ? `${amountFormatter(ethPrice, 18, 5)}` : '0.00000'}
                    {/* <span className="ml-2">
                    ($ {dollarPrice ? `${amountFormatter(dollarPrice, 18, 2)}` : '0.00'})
                  </span> */}
                  </p>
                </div>
                <div className="flex flex-col flex-wrap mb-4 max-w-22  transform scale-90 xxs:scale-100">
                  <BuyButton className="mr-4 mb-2" />
                  {balanceWINE?.gt(0) ? <SellButton className="mr-4 mb-2" balanceWINE={balanceWINE} /> : null}

                  {balanceWINE?.gt(0) ? <ClaimButton className="mr-4 mb-2" balanceWINE={balanceWINE} /> : null}
                  {rIF(((redeemedCount && balanceUNIWINE?.gt(redeemedCount)) ?? false) || (balanceUNIWINE?.gt(0) && redeemedCount === null),
                    <ShipButton className="mr-4 mb-2 h-10" balanceUNIWINE={balanceUNIWINE} pending={redeemedCount === null} />)}
                </div>
              </>
              : <div>Fetching data from Ethereum...</div>}
            <div className="mb-4 text-sm hidden">
              Delivered on demand.<br/>
              <a
                href="/"
                onClick={e => {
                  e.preventDefault()
                  setAppState(state => ({ ...state, visible: !state.visible }))
                }}
              >
                Learn more
              </a>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>

    {/* Gradient */}
    <div className="gradient-bottom h-3/4 absolute w-full bg-gradient-to-t from-black bg-opacity-75 bottom-0 left-0" style={{ zIndex: '-1' }}></div>

    <div className="absolute z-40 w-full h-real-screen pointer-events-none top-0">
      <div className="relative w-full h-full">
        <Modal
          ethData={ethData}
          showConnect={showConnect}
          setShowConnect={setShowConnect}
        />
        { !showStatus
          ? null
          : account
            ? (
              <NavModal>
                <Status className="mb-8" ethData={ethData} />
                <Stats />
              </NavModal>
              )
            : <NavModal>
              <TitleH2 m="6">WINE Stats</TitleH2>
              <Stats />
            </NavModal>
          }
        { !showFAQ
          ? null
          : <NavModal>
            <Faq className="my-1" />
          </NavModal>
          }
      </div>
    </div>
  </>
  )
}
