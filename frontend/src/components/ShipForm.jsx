import { useEffect, useState } from 'react'
import { ButtonTW } from './ButtonTW'

// eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
// function encode (data) {
//   return Object.keys(data)
//     .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
//     .join('&')
// }

const name = 'name'
const line1 = 'line1'
const line2 = 'line2'
const city = 'city'
const state = 'state'
const zip = 'zip'
const country = 'country'
const email = 'email'
const timestamp = 'timestamp'

// map from variables to display text for each field
const nameMap = {
  [name]: 'Full Name',
  [line1]: 'Address Line 1',
  [line2]: 'Address Line 2',
  [city]: 'City',
  [state]: 'State / Province / Region',
  [zip]: 'ZIP / Postal code',
  [country]: 'Country',
  [email]: 'Email',
  [timestamp]: 'Time',
}

// the order for fields that will be submitted
// const nameOrder = [name, line1, line2, city, state, zip, country, email]

// default for each form field
const defaultState = {
  // [bot]: '',
  [name]: '',
  [line1]: '',
  [line2]: '',
  [city]: '',
  [state]: '',
  [zip]: '',
  [country]: '',
  [email]: '',
}

export default function RedeemForm ({ setUserAddress, setHasConfirmedAddress }) {
  // const [autoAddress/* , setAutoAddress */] = useState([])
  // const [inputY, setInputY] = useState(0)
  // const suggestEl = useRef()

  const [formState, setFormState] = useState(defaultState)

  const handleChange = (event) => {
    const { name, value } = event.target
    setFormState(state => ({ ...state, [name]: value }))
  }

  useEffect(() => setUserAddress(
    `${formState[name]}\n`
      + `${formState[line1]}${formState[line2] ? `\n${formState[line2]}` : ''}\n`
      + `${formState[zip]} ${formState[city]}${formState[state] ? ` ${formState[state]}` : ''}\n`
      + `${formState[country]}\n`
      + `${formState[email]}`,
  ), [formState, setUserAddress])

  const canSign
    = formState[name]
    && formState[line1]
    && formState[city]
    // && formState[state]
    && formState[zip]
    && formState[country]
    && formState[email]

  // if (canSign) {
  //   console.log('all good')
  // } else {
  //   console.log(!!formState[name],
  //     !!formState[line1],
  //     !!formState[city],
  //     // !!formState[state],
  //     !!formState[zip],
  //     !!formState[country],
  //     !!formState[email])
  // }

  // TODO: apply best practices for address form fields: https://web.dev/payment-and-address-form-best-practices/
  return (
    <form className="form-frame mb-5 w-full flex flex-row items-center justify-stretch">
      <input
        required
        type="text"
        name={name}
        value={formState[name]}
        onChange={handleChange}
        placeholder={nameMap[name]}
        autoComplete="name"
      />
      <input
        type="text"
        name={line1}
        value={formState[line1]}
        onChange={handleChange}
        placeholder={nameMap[line1]}
        autoComplete="address-line1"
      />

      <input
        type="text"
        name={line2}
        value={formState[line2]}
        onChange={handleChange}
        placeholder={nameMap[line2]}
        autoComplete="address-line2"
      />

      <span className="flex flex-row flex-nowrap">
        <input
          style={{ marginRight: '8px' }}
          required
          type="text"
          name={zip}
          value={formState[zip]}
          onChange={handleChange}
          placeholder={nameMap[zip]}
          autoComplete="postal-code"
        />
        <input
          required
          type="text"
          name={city}
          value={formState[city]}
          onChange={handleChange}
          placeholder={nameMap[city]}
          autoComplete="address-level2"
        />
      </span>
      <input
        type="text"
        name={state}
        value={formState[state]}
        onChange={handleChange}
        placeholder={nameMap[state]}
        autoComplete="address-level1"
      />

      <input
        required
        type="text"
        name={country}
        value={formState[country]}
        onChange={handleChange}
        placeholder={nameMap[country]}
        autoComplete="country-name"
      />

      <input
        required
        type="email"
        name={email}
        value={formState[email]}
        onChange={handleChange}
        placeholder={nameMap[email]}
        autoComplete="email"
      />
      <ButtonTW
        type="submit"
        className="w-full mt-2"
        disabled={!canSign}
        onClick={() => setHasConfirmedAddress(true)}
      >
        {canSign ? 'Next' : 'Complete the form'}
      </ButtonTW>
    </form>
  )
}
