import { ArrowBack } from '@material-ui/icons'
import { FC, HTMLAttributes } from 'react'
import { extendClasses } from '../utils/react-utils'

export const ModalBack: FC<HTMLAttributes<HTMLDivElement>> = ({ className, ...restProps }) => (
  <div className={extendClasses(className, 'p-2 cursor-pointer flex items-center')} {...restProps}>
    <ArrowBack className="mr-2"/> back
  </div>
)
