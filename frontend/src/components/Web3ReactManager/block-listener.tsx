import { Web3Provider } from '@ethersproject/providers'
import debounce from 'lodash/debounce'
import { useSnackbar } from 'notistack'
import { useCallback, useEffect, useMemo, createContext, useState } from 'react'
import useWindowFocus from 'use-window-focus'
import { useAppContext } from '../../context'
import { useActiveWeb3React } from '../../hooks/web3'

export const EthBlockContext = createContext<Number|null>(null)

export function EthBlockProvider ({ children }) {
  const block = useBlockListener()

  return <EthBlockContext.Provider value={ block }>{children}</EthBlockContext.Provider>
}

function useBlockListener () {
  const { appState } = useAppContext()
  const { library } = useActiveWeb3React()

  // Block tracker
  const isWindowInFocus = useWindowFocus()
  const [block, setBlock] = useState<Number|null>(null)
  const noUpdate = appState.noUpdate || !isWindowInFocus
  useActualBlockListener(library, setBlock, noUpdate)
  useShowInfoWhenUpdatePaused(isWindowInFocus, appState.noUpdate || !!process)

  return block
}

// TODO: could be refactored to https://swr.vercel.app/
function useActualBlockListener (library: Web3Provider|undefined, updateAppState: (change: any) => void, noUpdate: boolean) {
  const blockListener = useCallback((blockNumber: Number): void => {
    console.log('New block:', blockNumber)
    updateAppState({ block: blockNumber })
  }, [updateAppState])
  // Metamask often lags behind a block, and then send two block events directly after another
  const debouncedBlockListener = useMemo(() => debounce(blockListener, 500), [blockListener])

  // Attach block listener to provider
  useEffect(() => {
    if (library != null && !noUpdate) {
      // Since the listener is not called on intialization
      library.getBlockNumber().then(
        debouncedBlockListener,
        err => console.error('Failed to get blockNum initially', err),
      )

      library.on('block', debouncedBlockListener)
      console.log('[BlockListener] enabled')
      return () => {
        console.log('[BlockListener] disabled')
        library.removeListener('block', debouncedBlockListener)
      }
    // } else {
    //   updateAppState({ block: null })
    }
  }, [debouncedBlockListener, library, noUpdate, updateAppState])

  // Not a good idea... (doesnt improve and might even produce non-linear block numbers)
  // useEffect(() => {
  //   if (!library) return
  //   // eslint-disable-next-line @typescript-eslint/no-misused-promises
  //   const interval = setInterval(async () => await library.getBlockNumber().then(blockListener), 5000)
  //   return () => clearInterval(interval)
  // }, [library, blockListener])
}

function useShowInfoWhenUpdatePaused (isWindowInFocus: boolean, ignore: boolean) {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  useEffect(() => {
    if (!isWindowInFocus && !ignore) {
      const key = enqueueSnackbar('Updating is paused while not in focus', {
        persist: true,
      })
      return () => closeSnackbar(key)
    }
  }, [ignore, closeSnackbar, enqueueSnackbar, isWindowInFocus])
}
