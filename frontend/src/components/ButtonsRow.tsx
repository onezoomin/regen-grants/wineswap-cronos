import { FC, HTMLAttributes } from 'react'
import { extendClasses } from '../utils/react-utils'

export const ButtonsRow: FC<HTMLAttributes<HTMLDivElement>> = ({ className, children, ...restProps }) => (
  <div className={extendClasses(className, 'flex flex-wrap justify-center gap-2')} {...restProps}>
    {children}
  </div>
)
