import { ButtonHTMLAttributes, FC, useState } from 'react'
import { rIF } from '../utils/react-utils'
import { LoaderOverlay } from './Loader'
import { BouncingEllipsis } from './minis'

// TODO use https://tailwindcss.com/docs/hover-focus-and-other-states#disabled

export const ButtonTW: FC<ButtonHTMLAttributes<HTMLButtonElement> & {
  buttonType?: string
  pending?: boolean
  onClickWithLoad?: (e: any) => Promise<void>
  preventDefault?: boolean
}> = ({
  className = '',
  children,
  buttonType,
  disabled,
  onClick,
  onClickWithLoad,
  preventDefault = true,
  pending,
  ...rest
}) => {
  let classes = 'py-1 px-2 ' // padding
  classes += 'relative flex justify-center items-center text-center ' // spacing
  classes += 'uppercase rounded text-lg tracking-widest ' // style
  // if (!className.includes(" h-")) classes += 'h-fit'

  const [clickPending, setClickPending] = useState(false)
  if (onClick && onClickWithLoad) throw new Error("Can't have both onClick types")

  const hoverFocusClasses = 'transform duration-150 hover:scale-102 focus:scale-102 '
  if (disabled) {
    if (buttonType === 'pending') {
      classes += 'bg-transparent border-none '
    } else {
      classes += 'bg-transparent text-gray-50 border-gray-50 border-2 border-solid opacity-50 '
    }
  } else if (buttonType === 'primary') {
    classes += 'bg-gray-50 border-gray-50 border-2 border-solid text-gray-800 '
    classes += 'hover:bg-white focus:bg-white ' + hoverFocusClasses
  } else if (buttonType === 'outline') {
    classes += 'bg-transparent bg-opacity-0 text-white border-white border-2 border-solid '
    classes += 'hover:bg-black focus:bg-black hover:bg-opacity-30 focus:bg-opacity-30 ' + hoverFocusClasses
  } else {
    classes += 'bg-gray-50 border-gray-50 border-2 border-solid text-gray-800 opacity-80 '
    classes += 'hover:bg-white focus:bg-white ' + hoverFocusClasses
  }
  if (className) { classes += className }

  return (
    <button
      className={classes}
      disabled={clickPending || disabled}
      {...rest}

      onClick={e => {
        if (preventDefault) {
          e.preventDefault()
        }
        if (clickPending || disabled) return

        if (onClickWithLoad) {
          setClickPending(true)
          onClickWithLoad?.(e)
            .then(() => setClickPending(false)) // TODO: skip when unmounted
        } else if (onClick) {
          onClick?.(e)
        }
      }}
    >
      { children }
      { rIF(pending, <BouncingEllipsis className={children ? 'ml-4' : ''} />) }
      { rIF(clickPending, <LoaderOverlay />) }
    </button>
  )
}
